<?php

namespace ElvisLicsari\Csv;

class Csv
{
    public static function getFileHeaders($filePath, $delimiter = ',', $number = 1)
    {
        $lines   = self::getLinesFromFile($filePath, $number);
        $headers = self::getFileHeaderFromLines($lines, $delimiter);
        return $headers;
    }

    public static function getLinesFromFile($filePath, $number = 1)
    {
        $lines  = [];
        $handle = fopen($filePath, "r");
        if ($handle) {
            $lineNumber = 0;
            while (($line = fgets($handle)) !== false) {
                $lines[$lineNumber] = $line;
                $lineNumber++;
                if ($lineNumber >= $number)
                    break;
            }
            fclose($handle);
        }
        return $lines;
    }

    public static function getFileHeaderFromLines($lines, $delimiter)
    {
        $headers = explode($delimiter, strtolower(trim($lines[0])));
        return $headers;
    }

    public static function toCsv($results, $preContent = "", $postContent = "")
    {
        $csv = $preContent."\n";
        foreach ($results as $result)
            $csv .= implode(",", array_map(function($value) {return '"'.$value.'"';}, array_values($result)))."\n";
        $csv .= $postContent;
        return $csv;
    }

    public static function csvToArray($csvPath, $skipHeader = true)
    {
        $lines      = [];
        $fp         = fopen($csvPath, "r");
        $i=0;
        while (($line = fgetcsv($fp)) !== FALSE) {
            $i++;
            if($i==1 && $skipHeader)
                continue;
            $lines[] = $line;
        }
        fclose($fp);
        return $lines;
    }

    public static function toXls($results, $preContent = "", $postContent = "")
    {
        $csv = $preContent."\r\n";
        foreach ($results as $result)
            $csv .= implode("\t", array_map(function($value) {return $value;}, array_values($result)))."\r\n";
        $csv .= $postContent;
        return $csv;
    }
}