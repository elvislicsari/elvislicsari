<?php

namespace ElvisLicsari\Logging;

class Logger
{
    private static $instance;
    private static $pre;

    protected function __construct($pre = "")
    {
        self::$pre = $pre;
    }

    public static function log($message)
    {
        $instance = self::init();
        $instance->logMessage($message);
    }

    public static function init($pre = "")
    {
        if (self::$instance == null)
            self::$instance = new Logger($pre);
        return self::$instance;
    }

    protected function logMessage($message)
    {
        if (empty(self::$pre))
            echo date("Y-m-d H:i:s")." -> ".$message."\n";
        else
            echo date("Y-m-d H:i:s")." ".self::$pre." -> ".$message."\n";
    }
} 