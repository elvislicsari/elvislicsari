<?php

namespace ElvisLicsari\Countdown;

interface Countdownable extends CountdownStartable, CountdownVerifiable
{
}