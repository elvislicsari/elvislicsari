<?php

namespace ElvisLicsari\Countdown;

interface CountdownVerifiable
{
    public static function verifyCountdown();
} 