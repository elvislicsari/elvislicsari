<?php

namespace ElvisLicsari\Countdown;

class Countdown implements Countdownable
{
    private static $startTime;
    private static $runForSeconds;

    public static function startCountdown($runForSeconds = 60)
    {
        self::$startTime = time();
        self::$runForSeconds = $runForSeconds;
    }

    public static function isExpired()
    {
        if (time() - self::$startTime > self::$runForSeconds)
            return true;
        return false;
    }

    public static function verifyCountdown()
    {
        if (self::isExpired())
            throw new CountdownExpiredException();
    }
} 