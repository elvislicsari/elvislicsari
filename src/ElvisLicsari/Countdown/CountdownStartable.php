<?php

namespace ElvisLicsari\Countdown;

interface CountdownStartable 
{
    public static function startCountdown($runForSeconds = 60);
} 