<?php

namespace ElvisLicsari\Countdown;
use Exception;

class CountdownExpiredException extends Exception
{
    public function __construct() {
        parent::__construct("Countdown expired");
    }
} 