<?php

use ElvisLicsari\Countdown\Countdown;

class CountdownTest extends PHPUnit_Framework_TestCase {

    public function testIsExpiredSuccess()
    {
        Countdown::startCountdown(2);
        $this->assertFalse(Countdown::isExpired());
        sleep(1);
        $this->assertFalse(Countdown::isExpired());
        sleep(2);
        $this->assertTrue(Countdown::isExpired());
    }

    /**
     * @expectedException        ElvisLicsari\Countdown\CountdownExpiredException
     * @expectedExceptionMessage Countdown expired
     */
    public function testVerifyCountdown()
    {
        Countdown::startCountdown(2);
        sleep(3);
        Countdown::verifyCountdown();
    }
}
 